# this code is for idea only, in production every class will be placed into different file only

import uuid
from enum import Enum
from time import time

MAX_LIMIT = 100


class Cuisine(Enum):
    SouthIndian = "SouthIndian"
    NorthIndian = "NorthIndian"
    Chinese = "Chinese"


class CostBucket(Enum):
    one = "0"
    two = "1"
    three = "2"
    four = "3"
    five = "4"


class Restaurant:

    def __init__(self, cuisine, cost_bracket, rating: float = 0.0, is_recommended: bool = False):
        self.id = str(uuid.uuid4())
        self.cuisine = cuisine
        self.cost_bracket = cost_bracket
        self.rating = rating
        self.is_recommended = is_recommended
        self.onboarded_time = time()


class CuisineTracking:

    def __int__(self, cuisine_type: Cuisine, no_of_orders: int = 0):
        self.cuisine_type = cuisine_type
        self.no_of_orders = no_of_orders

    def increase_no_of_order(self):
        self.no_of_orders += 1


class CostTracking:

    def __int__(self, cost_type: CostBucket, no_of_orders: int = 0):
        self.cost_type = cost_type
        self.no_of_orders = no_of_orders

    def increase_no_of_order(self):
        self.no_of_orders += 1


class User:

    def __init__(self):
        self.id = str(uuid.uuid4())
        self._cuisine_tracking = dict()
        self._cost_tracking = dict()
        self.secondary_cost_bracket = []  # maintaining as list so currently we have two secondary.
        # in the future, there might be more than two secondary
        self.primary_cost_bracket = None
        self.secondary_cuisine = []  # maintaining as list so currently we have two secondary.
        # in the future, there might be more than two secondary
        self.primary_cuisine = None

    def update_primary_and_secondary_cuisine(self):
        sorted_cuisine = sorted(self._cuisine_tracking.items(), key=lambda x: x[1])
        self.primary_cuisine = sorted_cuisine[0][0]

        if len(sorted_cuisine) > 1:
            self.secondary_cuisine = [i[0] for i in sorted_cuisine[1:2]]

    def update_primary_and_secondary_cost_bracket(self):
        sorted_cost_bucket = sorted(self._cost_tracking.items(), key=lambda x: x[1])
        self.primary_cost_bracket = sorted_cost_bucket[0][0]

        if len(sorted_cost_bucket) > 1:
            self.secondary_cost_bracket = [i[0] for i in sorted_cost_bucket[1:2]]

    def add_cuisine_and_cost_from_restaurant(self, restaurant: Restaurant):
        cuisine = restaurant.cuisine
        cost_bracket = restaurant.cost_bracket

        if not self._cuisine_tracking.get(cuisine):
            self._cuisine_tracking[cuisine] = 0
        self._cuisine_tracking[cuisine] += 1

        if not self._cost_tracking.get(cost_bracket):
            self._cost_tracking[cost_bracket] = 0
        self._cost_tracking[cost_bracket] += 1


class RestaurantRecommendationSystem:

    def __init__(self, user: User, restaurants: list):
        self.restaurants = restaurants
        self.user = user
        self.recommended_restaurants = []
        self.restaurant_map = {"primary": [], "secondary": []}
        self.unique_restaurant_map = {}

    def _pre_processing(self):
        self.user.update_primary_and_secondary_cuisine()
        self.user.update_primary_and_secondary_cost_bracket()

        # restaurant map based on cuisine as our current algo mainly has first level filter on cuisine
        # for future cases, other pre-processing can be done to avoid processing same data
        for restaurant in self.restaurants:
            if restaurant.cuisine == self.user.primary_cuisine:
                self.restaurant_map["primary"].append(restaurant)
            elif restaurant.cuisine in self.user.secondary_cuisine:
                self.restaurant_map["secondary"].append(restaurant)

    def _filter_restaurants(self, **kwargs):
        result = []

        if kwargs.get("primary"):
            _restaurants = self.restaurant_map["primary"]
        elif not kwargs.get("primary"):
            _restaurants = self.restaurant_map["secondary"]
        else:
            _restaurants = self.restaurants

        kwargs.pop("primary", None)

        for rest in _restaurants:
            status = True
            for k, v in kwargs.items():
                res_dict = rest.__dict__
                if isinstance(v, tuple):
                    if v[0] == ">":
                        if res_dict.get(k) <= v[1]:
                            status = False
                    elif v[0] == "<":
                        if res_dict.get(k) >= v[1]:
                            status = False
                    elif v[0] == ">=":
                        if res_dict.get(k) < v[1]:
                            status = False
                    elif v[0] == "<=":
                        if res_dict.get(k) > v[1]:
                            status = False
                    elif v[0] == "=":
                        if res_dict.get(k) != v[1]:
                            status = False
                if isinstance(v, list):
                    if res_dict.get(k) not in v:
                        status = False
                else:
                    if res_dict.get(k) != v:
                        status = False

                if status:
                    if rest.id not in self.unique_restaurant_map:
                        self.unique_restaurant_map[rest.id] = True
                        result.append(rest)

        return result

    # every filter is separate, so that in near future we can add new strategy and remove one

    def get_featured_restaurants_with_primary_cuisine_and_cost_bracket_filter_1(self):

        response = self._filter_restaurants(primary=True, is_recommended=True,
                                            cost_bracket=self.user.primary_cost_bracket)
        if not response:
            response = self._filter_restaurants(primary=True, cost_bracket=self.user.secondary_cost_bracket)

            self.recommended_restaurants.extend(response)

            response = self._filter_restaurants(primary=False, cost_bracket=self.user.primary_cost_bracket)

        self.recommended_restaurants.extend(response)

    def get_primary_cuisine_primary_cost_bracket_filter_2(self):
        response = self._filter_restaurants(primary=True, cost_bracket=self.user.primary_cost_bracket, rating=(">", 4))

        self.recommended_restaurants.extend(response)

    def get_primary_cuisine_secondary_cost_bracket_filter_3(self):
        response = self._filter_restaurants(primary=True, cost_bracket=self.user.secondary_cost_bracket,
                                            rating=(">=", 4.5))

        self.recommended_restaurants.extend(response)

    def get_secondary_cuisine_primary_cost_bracket_filter_4(self):
        response = self._filter_restaurants(primary=False, cost_bracket=self.user.primary_cost_bracket,
                                            rating=(">=", 4.5))

        self.recommended_restaurants.extend(response)

    def get_secondary_cuisine_secondary_cost_bracket_filter_5(self):
        new_create_timestamp = time() - 48 * 3600
        response = self._filter_restaurants(onboarded_time=(">", new_create_timestamp))
        response = sorted(response, key=lambda x: x.rating, reverse=True)[:4]
        self.recommended_restaurants.extend(response)

    def get_primary_cuisine_primary_cost_bracket_filter_6(self):
        response = self._filter_restaurants(primary=True, cost_bracket=self.user.primary_cost_bracket, rating=("<", 4))

        self.recommended_restaurants.extend(response)

    def get_primary_cuisine_secondary_cost_bracket_filter_7(self):
        response = self._filter_restaurants(primary=True, cost_bracket=self.user.secondary_cost_bracket,
                                            rating=("<", 4.5))

        self.recommended_restaurants.extend(response)

    def get_secondary_cuisine_primary_cost_bracket_filter_8(self):
        response = self._filter_restaurants(primary=False, cost_bracke_t=self.user.primary_cost_bracket,
                                            rating=("<", 4.5))

        self.recommended_restaurants.extend(response)

    def all_restaurants_filter_9(self):
        if len(self.recommended_restaurants) < MAX_LIMIT:
            n = MAX_LIMIT - len(self.recommended_restaurants)
            count = 0
            for i in self.restaurants:
                if count == n:
                    break
                if i.id not in self.unique_restaurant_map:
                    self.recommended_restaurants.append(i)
                    self.unique_restaurant_map[i.id] = True
                    count += 1

    def get_all_recommended_restaurants(self):
        self._pre_processing()
        self.get_featured_restaurants_with_primary_cuisine_and_cost_bracket_filter_1()
        self.get_primary_cuisine_primary_cost_bracket_filter_2()
        self.get_primary_cuisine_secondary_cost_bracket_filter_3()
        self.get_secondary_cuisine_primary_cost_bracket_filter_4()
        self.get_secondary_cuisine_secondary_cost_bracket_filter_5()
        self.get_primary_cuisine_primary_cost_bracket_filter_6()
        self.get_primary_cuisine_secondary_cost_bracket_filter_7()
        self.get_secondary_cuisine_primary_cost_bracket_filter_8()
        self.all_restaurants_filter_9()
        return self.recommended_restaurants


def driver():
    restaurants = list()

    user_1 = User()
    restaurant_1 = Restaurant(cuisine=Cuisine.NorthIndian.value, cost_bracket=CostBucket.one.value, rating=4.3,
                              is_recommended=True)
    restaurant_2 = Restaurant(cuisine=Cuisine.SouthIndian.value, cost_bracket=CostBucket.two.value, rating=4,
                              is_recommended=True)
    restaurant_3 = Restaurant(cuisine=Cuisine.SouthIndian.value, cost_bracket=CostBucket.two.value, rating=2.3,
                              is_recommended=True)
    restaurant_4 = Restaurant(cuisine=Cuisine.Chinese.value, cost_bracket=CostBucket.one.value, rating=4,
                              is_recommended=True)

    restaurants.append(restaurant_1)
    restaurants.append(restaurant_2)
    restaurants.append(restaurant_3)
    restaurants.append(restaurant_4)
    user_1.add_cuisine_and_cost_from_restaurant(restaurant_3)

    obj = RestaurantRecommendationSystem(user=user_1, restaurants=restaurants)
    recommended_restaurants = obj.get_all_recommended_restaurants()

    for res in recommended_restaurants:
        print(res.__dict__)


if __name__ == "__main__":
    driver()
